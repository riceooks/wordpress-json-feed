<?php

// WordPress config
require (dirname(__FILE__) . '/wp-config.php');

// Include file
require_once (ABSPATH . "/wp-includes/class-json.php");
require_once (ABSPATH . "/wp-includes/class-barry.php");

// Global variable
$json_obj = new Services_JSON();
$barry = new WP_Barry();

// Params
$action = isset($_GET['action']) ? trim($_GET['action']) : 'default';
$cat_id = isset($_GET['cat_id']) && intval($_GET['cat_id']) > 0 ? intval($_GET['cat_id']) : '';
$post_id = isset($_GET['post_id']) && intval($_GET['post_id']) > 0 ? intval($_GET['post_id']) : 0;
$page_id = isset($_GET['page_id']) && intval($_GET['page_id']) > 0 ? intval($_GET['page_id']) : 0;
$page = isset($_GET['page']) && intval($_GET['page']) > 1 ? intval($_GET['page']) : 1;
$limit = isset($_GET['limit']) && intval($_GET['limit']) > 0 ? intval($_GET['limit']) : 10;
$parent = isset($_GET['parent']) && intval($_GET['parent']) > 0 ? intval($_GET['parent']) : 0;

switch( $action ) {
    /** 分類 XXX
     * @param String $action 'categories'
     * @param Number $parent default 0
     */
    case 'categories' :
        $args = array('type' => 'post', 'parent' => $parent, 'orderby' => 'ID', 'order' => 'ASC', 'hide_empty' => 0, 'taxonomy' => 'category');
        $categories = get_categories($args);

        $results = array('result' => true, 'categories' => $categories, 'count' => count($categories));

        echo $barry -> json_format($json_obj -> encode($results));
        break;

    /** 文章列表 XXX
     * @param String $action 'posts'
     * @param Number $page default 1
     * @param Number $limit default 10
     * @param Number $cat_id default ''
     */
    case 'posts' :
        $args = array('posts_per_page' => $limit, 'offset' => ($page - 1) * $limit, 'category' => $cat_id, 'orderby' => 'ID', 'order' => 'ASC', 'post_type' => 'post', 'post_status' => 'publish', 'suppress_filters' => true);
        $posts = get_posts($args);
		
		if( $cat_id ){
			$count = get_category( $cat_id )->count;
		} else {
        	$count = wp_count_posts('post') -> publish;
		}
        $results = array('result' => true, 'posts' => $posts, 'page' => $page, 'limit' => $limit, 'count' => $count);

        // echo $barry -> json_format($json_obj -> encode($results));
		echo $json_obj -> encode($results);
        break;

    /** 單篇文章 XXX
     * @param String $action 'post'
     * @param Number $post_id
     */
    case 'post' :
        $post = get_post($post_id, OBJECT);

        if (!empty($post)) {
            $results = array('result' => true, 'post' => $post);
        } else {
            $results = array('result' => false);
        }

        // echo $barry -> json_format($json_obj -> encode($results));
		echo $json_obj -> encode($results);
        break;

    /** 頁面列表 XXX
     * @param String $action 'pages'
     * @param Number $page default 1
     * @param Number $limit default 10
     * @param Number $parent default 0
     */
    case 'pages' :
        $args = array('sort_order' => 'ASC', 'sort_column' => 'ID', 'parent' => $parent, 'exclude_tree' => '', 'number' => $limit, 'offset' => ($page - 1) * $limit, 'post_type' => 'page', 'post_status' => 'publish');
        $pages = get_pages($args);

        $count = wp_count_posts('page') -> publish;
        $results = array('result' => true, 'pages' => $pages, 'page' => $page, 'limit' => $limit, 'count' => $count);

        echo $barry -> json_format($json_obj -> encode($results));
        break;

    /** 單篇頁面 XXX
     * @param String $action 'page'
     * @param Number $page_id
     */
    case 'page' :
         $page = get_page($post_id, OBJECT);

        if (!empty($page)) {
            $results = array('result' => true, 'page' => $page);
        } else {
            $results = array('result' => false);
        }

        echo $barry -> json_format($json_obj -> encode($results));
        break;
    default :
        header('Location: index.php');
        break;
}
?>